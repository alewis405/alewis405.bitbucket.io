# -*- coding: utf-8 -*-
"""
@file Lab_3_Computer.py
@brief This script lets the user request data and then receives, plots, and stores the data as a csv file. 
@details This script first asks the user to press G in order to start data collection. It accepts 'G' and 'g'. Then it continually checks if the data it recieves is in a batch of a length greater than or less than 10. Once it gets a list greater than 10, it parses the analog data by using the ',' characters sent from the UART. Then the analog values are converted to voltage. The voltage, analog, and time values are plotted using matplotlib.
@author Alexander Lewis
"""

import serial
import matplotlib.pyplot as plt
import csv

class User_Interface:
    '''
    @brief This user interface requests and receives data from the Nucleo when 'G' or 'g' is pressed. Then it plots the data.
    @date 1/26/2021
    @author Alexander Lewis
    '''
    ## @brief Establishes UART connection. 
    ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
    ## @brief Home state asks the user to enter 'g' or 'G'
    S0_Home = 0
    ## @brief Data state includes all the data handling functions. 
    S1_Data = 1
    
    def __init__(self):
        '''
        @brief This initializer simply sets the state to home.
        @author Alexander Lewis
        '''
      
        ## @brief Sets the initial state 
        self.state = self.S0_Home

    def run(self):
        '''
        @brief This run method runs one iteration through the Finite State Machine.
        @author Alexander Lewis
        '''
        
        if (self.state == self.S0_Home):
            user_input = input('Please enter G to collect data: ')
            if (user_input == 'G') or (user_input == 'g'):
                self.transitionTo(self.S1_Data)
            else:
                print('That is not a recognized character.')
                    
        elif (self.state == self.S1_Data):
            analog = []
            t_list = []
            voltage = []
            
            t = 0
            data_point = None
            data_point = self.readline()
            if len(data_point) > 10:
                
                data_point = str(data_point)
                data_point = data_point.split(',')
                for n in data_point: 
                    analog.append(n)
                    t_list.append(t)
                    t += 5
                analog[0] = 0
                plt.plot(t_list,analog)
                plt.ylabel('Analog Reading')
                plt.xlabel('Time [Microseconds]')
                plt.show()
                for n in voltage:
                    volts = .00080586*n
                    voltage.append(volts)
                plt.plot(t_list,analog)
                plt.ylabel('Volts')
                plt.xlabel('Time [Microseconds]')
                plt.show()
                
                
                with open('Lab_3.csv', 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(t_list)
                    writer.writerow(voltage)
                    writer.writerow(analog)
                self.transitionTo(self.S0_Home)

                
                
                

    def transitionTo(self,State):
        self.state = State

    def sendChar(self,inv):
        '''
        @brief This method sends a character over the UART.
        @date 1/26/2021
        @author Alexander Lewis
        '''
        self.ser.write(str(inv).encode('ascii'))


    def readline(self):
        '''
        @brief This method reads and returns a line from the UART.
        @date 1/26/2021
        @author Alexander Lewis
        '''
        myval = self.ser.readline()
        return myval
    
interface = User_Interface()
while True:
    interface.run()

    