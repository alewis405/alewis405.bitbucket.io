# -*- coding: utf-8 -*-
"""
@file Lab_2.py
@brief This file runs on the Nucleo Microcontroller and measures reaction time. 
@details This file turns on an LED on the Nucleo after some random 2-3 second interval. Then it measures the time it takes for the user to press the interupt button and it reports reaction time. 
@date 1/23/2021
@author Alexander Lewis
"""

import random
import pyb
import utime

class Reaction_Timer:
    '''
    @brief This class creates the Reaction Timer object.  
    @details This reaction timer tests the user by turning on an LED after a random amount of time. Then it measures how long it takes for the user to press the blue button on the Nucleo.
    @author Alexander Lewis
    @date 1/24/2021
    '''


    def __init__(self):
        '''
        @brief This initializer defines the reaction tester system
        @date 1/23/2021
        @author Alexander Lewis
        '''
        ## @brief This sets up the LED pin.
        self.LED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
        
        ## @brief This sets up the timer.
        self.timer = pyb.Timer(2,prescaler = 49, period = 80000000)
        
        ## @brief Initializes isr count
        self.isr_count = 0


    def run(self):
        '''
        @brief This method runs once every time this module is turned on.
        @date 1/23/2021
        @author Alexander Lewis
        '''

        
        print("GET READY TO PRESS THE BLUE BUTTON AS SOON AS THE LED TURNS ON!!!")
        
        ## @brief This sets the delay from the start message to when the light will turn on.
        delay = 2 + .1*random.randrange(2,11)
        utime.sleep(delay)
        
        ## @brief Resets ISR count to zero just before the light turns on
        self.isr_count = 0 
        
        ## @ brief The reaction start time starts as soon as the LED turns on. It also converts to microseconds.
        reaction_start_time = self.timer.counter()
        
        ## @brief Turns LED ON
        self.LED_On()
        
        ## @brief This loop waits until the button is pressed or until 10 seconds has passed since the LED turned on. 
        while self.isr_count == 0:
            if (self.timer.counter()*50*12.5/1000000000 - reaction_start_time*50*12.5/1000000000) >=10:
                print('You have taken too long to press the button. Please try again.')
                self.isr_count = 0
                break
        ## @brief LED turns off when the button is pressed.    
        self.LED_Off()
        
        if self.isr_count == 0:
            pass
        else:
            reaction_end_time = self.timer.counter()
            reaction_time = reaction_end_time - reaction_start_time
            if reaction_time < 0:
                reaction_time += 80000000
            reaction_time = reaction_time *50*12.5/1000
            print('Your reaction time was ' + str(reaction_time) + ' Microseconds. This is equal to ' + str(round(reaction_time/1000000 ,3)) + ' seconds.')

                  

    def LED_On(self):
        '''
        @brief This method turns the LED on
        @date 1/23/2021
        @author Alexander Lewis
        '''
        self.LED.high()
        

    def LED_Off(self):
        '''
        @brief This method turns the LED on
        @date 1/23/2021
        @author Alexander Lewis
        '''
        self.LED.low()
    
    def count_isr (self, which_pin): 
        '''
        @brief This method adds to the ISR count when it is called.
        @date 1/24/2021
        @author Alexander Lewis
        '''
        self.isr_count += 1
        
## TESTER contains the reaction timer object.   
TESTER = Reaction_Timer()
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             TESTER.count_isr)     
## PC13 is the blue button pin. 
PC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)
TESTER.count_isr(PC13)

while True:
    TESTER.run()
    print('In 5 seconds the game will begin again!')
    utime.sleep(5)
    
    