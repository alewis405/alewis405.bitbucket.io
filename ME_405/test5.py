# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 22:47:50 2021

@author: Alexander Lewis
"""

import pyb
import utime

Yp = pyb.Pin(pyb.Pin.board.PA7, mode=pyb.Pin.OUT_PP)
Ym = pyb.Pin(pyb.Pin.board.PA6, mode=pyb.Pin.OUT_PP)
Xp = pyb.Pin(pyb.Pin.board.PA1, mode=pyb.Pin.IN)
Xm = pyb.Pin(pyb.Pin.board.PA0, mode=pyb.Pin.ANALOG)

adc = pyb.ADC(Xm)

Xp.high()
Yp.high()
Ym.low()
while True:
    utime.sleep(.1)
#    print(adc_reference.read_vref())
    print(adc.read())

    # print('adc reference:')
    # print(adc_reference.read())
    
