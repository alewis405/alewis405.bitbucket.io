# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 09:13:59 2021

@author: Alexander Lewis
"""
import pyb
from Motor import Encoder
from Motor import Driver
from Touch2 import Touch
import utime


class State: 
    
    def __init__(self,encoder1, encoder2, motor1, motor2,touch_sensor,K1,K2,K3,K4):
        self.encoder1 = encoder1
        self.encoder2 = encoder2
        self.motor1 = motor1
        self.motor2 = motor2
        self.sensor = touch_sensor
        self.last_position = (0,0,0)
        self.position = self.sensor.pos()
        
        self.last_time = utime.ticks_us()
        self.K1 = K1
        self.K2 = K2
        self.K3 = K3
        self.K4 = K4
        self.encoder1.update()
        self.encoder2.update()
        self.x_vel = 0
        self.y_vel = 0
        
    def ball(self):
        self.last_position = self.position
        self.position = self.sensor.pos()
        
        if self.position[2] == 1: 
            time_delta = utime.ticks_diff(utime.ticks_us(),self.last_time)
            self.x_vel = (self.position[0] - self.last_position[0])/(time_delta/100000)
            self.y_vel = (self.position[1] - self.last_position[1])/(time_delta/100000)
            self.last_time = utime.ticks_us()

        

    
    
    def Prop(self):
        
        A = 5
        B = 4
        D = 4
        C = 1
        x_dot = self.K1*A + self.K2*B
        y = self.K3*C + self.K4*D
        
        
        self.motor1.enable()
       # self.motor1.isr_scan()

        ## Error is in percentage
        x_er = (self.position[0]/88)*100
        
        y_er = (self.position[1]/50)*100
      
        if self.position[2] == 0:
            self.motor1.disable()
            
            
        else:
            self.motor1.enable()
            if x_er > 0:
                prop = self.Kp*x_er
                
                duty = prop
                if duty > 55:
                    duty = 55
                
                if encoder2.position < -5:
                    self.motor2.set_duty(20,1)

                self.motor2.set_duty(duty,0)

                
            else:
                prop = abs(self.Kp*x_er)
                duty = prop
                if duty > 55:
                    duty = 55
                    
                if encoder2.position > 5:
                    duty = 20
                    
                self.motor2.set_duty(duty,1)


            if y_er >0:  
                prop = self.Kp*y_er

                duty = prop
                if duty > 55:
                    duty = 55
                
                if encoder1.position < -5:
                    self.motor1.set_duty(20,1)

                self.motor1.set_duty(duty,0)


            else:
                prop = abs(self.Kp*y_er)
                
                duty = prop
                if duty > 55:
                    duty = 55
                if encoder1.position > 5:
                    duty = 20

                self.motor1.set_duty(duty,1)
            utime.sleep(.01)

            
        
        
        
encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4)
encoder2 = Encoder(pyb.Pin.cpu.C6,pyb.Pin.cpu.C7,8)
sleep_pin = pyb.Pin(pyb.Pin.cpu.A15)
motor1 = Driver(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,sleep_pin,3,1,2)
motor2 = Driver(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,sleep_pin,3,3,4)
motor2.enable()
motor1.enable()
sensor = Touch(100,176)
    
PB2 = pyb.Pin(pyb.Pin.board.PB2, mode = pyb.Pin.IN)
PC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

# extint1 = pyb.ExtInt (pyb.Pin.board.PB2,   # Which pin
#          pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#          pyb.Pin.PULL_UP,             # Activate pullup resistor
#          motor1.isr_1) 

# extint2 = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
#          pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#          pyb.Pin.PULL_UP,             # Activate pullup resistor
#          motor1.isr_2) 

 
Controller = State(encoder1,encoder2,motor1,motor2,sensor,.2,1)
while True:
    
    Controller.ball()
 #   Controller.Prop()
    Controller.Der()
    
    