# -*- coding: utf-8 -*-
"""
@file mcp9808.py
@brief This script gets data from the mcp9808 sensor.
@details This script contains modules to check sensor connection, celsius temperature, and fahrenheit temperature.
@author Alexander Lewis
"""
import pyb
import utime



class MCP9808:
    
    i2c1 = pyb.I2C (1, pyb.I2C.MASTER, baudrate = 100000)
    
    def __init__(self):
        self.i2c1 = pyb.I2C(1, pyb.I2C.MASTER)
        self.connection = False
        
    def check(self):
        '''
        @brief This method verifies that the sensor is attached at the given bus address.
        @author Alexander Lewis
        '''
        bus = self.i2c1.scan()
        
        if not bus:
            self.connection = False

        elif bus[0] == 24:
            self.connection = True
        else:
            pass
            
    def celsius(self):
        '''
        @brief This method returns the temperature in Celsius
        @author Alexander Lewis
        '''
        C = self.i2c1.mem_read(2, addr = 24 ,memaddr = 5)   # Read sensor data
        bit = self.translate_bit(bin(C[0]),bin(C[1]))
        
        return bit
    
    def fahrenheit(self):
        '''
        @brief This method returns the temperature in Fahrenheit
        @author Alexander Lewis
        '''
        temp = self.celsius()
        f = temp * (9/5) + 32
        
        return f

    
    def translate_bit(self, bit0, bit1):
        '''
        @brief This method parses the bits into a list and then calculates the temperature based on the tech spec 5.1 bit assignment table.
        @author Alexander Lewis

        '''
        bit_str0 = str(bit0)
        bit_str0.split()
        bit_str1 = str(bit1)
        bit_str1.split()
        bit_list1 = []
        bit_list0 = []
        for n in bit_str0[2:]:
            bit_list0.append(int(n))
        for m in bit_str1[2:]:
            bit_list1.append(int(m))
            
        output = bit_list0[4]*(2**7) + bit_list0[5]*(2**6)+ bit_list0[6]*(2**5)+ bit_list0[7]*(2**4)+ bit_list1[0]*(2**0)+ bit_list1[1]*(2**-1)+ bit_list1[2]*(2**-2)+ bit_list1[3]*(2**-3)+ bit_list1[4]*(2**-4)
        return output
    
    
if __name__ == "__main__":
    
    sensor = MCP9808()
    sensor.check()
    while True:
        if sensor.connection == True:
            print('Sensor Connected')
            C = sensor.celsius()
            print(str(C) + ' degrees Celsius' )
            F = sensor.fahrenheit()
            print(str(F) + ' degrees Fahrenheit')
        else:
            print('Not connected')
            
        utime.sleep_ms(1000)
        
        
    

