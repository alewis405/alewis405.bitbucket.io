# -*- coding: utf-8 -*-
"""
@file main.py

@brief This file collects data and continually creates a csv file. 

@author Alexander Lewis
"""

import mcp9808
import ADCALL
import pyb
import utime

LED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

LED.low()

start_time = utime.ticks_us()

sensor = mcp9808.MCP9808()

mcu = ADCALL.MCU_temp()

total_time = 0

with open ("Lab_4_time.csv", "w") as a_file:
        a_file.write("Time: {:}\r\n".format(total_time))
with open ("Lab_4_MCU.csv", "w") as a_file:
        a_file.write("MCU Temperature: {:}\r\n".format(0))
with open ("Lab_4_sensor.csv", "w") as a_file:
        a_file.write("Sensor Temperature: {:}\r\n".format(0))


while total_time < 28800000000:
    
    sensor.check()
    if sensor.connection == True:
        C = sensor.celsius()
        sensor.connection == False
    else:
        LED.low()
    
    mcu.adcall.read_vref()
    
    MCU = mcu.Mcu_t()
    
    # These delays and blinks are not necessary for the file function. They make it easy to check that the nucleo hasn't been disconnected and that the program is running smoothly. 
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    utime.sleep(5)
    LED.high()
    utime.sleep(5)
    LED.low()
    total_time = utime.ticks_diff(utime.ticks_us(),start_time)
    
    with open ("Lab_4_time.csv", "a") as a_file:
        a_file.write("{:}\r\n".format(total_time))
    with open ("Lab_4_MCU.csv", "a") as a_file:
        a_file.write("{:}\r\n".format(MCU))
    with open ("Lab_4_sensor.csv", "a") as a_file:
        a_file.write("{:}\r\n".format(C))

LED.high()
        
        
        
    