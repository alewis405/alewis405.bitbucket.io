"""
@file       encoder.py
@author     Alexander Lewis

@brief      This script contains both the encoder and driver classes for the motor. 
@details    This file creates an encoder object that reads the position and
            angular velocity of the motor. Additionally, "bad" deltas, ones
            caused by overflow or underflow are corrected.
            

"""
import pyb
import utime

class EncoderDriver:
    '''
    @brief This class tracks the position of the motor. 
    @author Alexander Lewis
    '''
    def __init__(self, pin1, pin2, timer):
        '''
        @brief This initializer takes two pins and the timer required to run the encoder.
        @author Alexander Lewis
        '''
        ## Copying the timer number
        self.timer = timer
        ## Copying the Pin 1 object
        self.pin1 = pin1
        ## Copying the Pin 2 object
        self.pin2 = pin2

        ## Creating a timer object
        self.tim = pyb.Timer(timer)
        
        self.tim.init(prescaler = 0,period =0xFFFF) #Setting the prescale and period of the timer
        
        self.tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB) # Initializing timer channel 1
        self.tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB) # Initializing timer channel 2
        

        self.count = 0
        ## Delta between two most recent readings
        self.delta = 0
        ## Current position of the encoder
        self.position = 0 
        self.last_count = 0
        self.last_delta = 0
        
        ## Maximum positive delta value before reading error
        self.DELTAMAX = 0xFFFF/2
        ## Minimum positive delta value before reading error
        self.DELTAMIN = -0xFFFF/2    

        ## Clockwise motion
        self.dir = 1
        ## Counterclockwise motion
        self.CCW = 0 
        ## Period of the timer
        self.PER = 0xFFFF
        ## Current time of the encoder
        self.time = utime.ticks_us()
    
    def update(self):
        '''
        @brief Every time this method is run it updates the position of the motor. 
        @author Alexander Lewis
        '''
        ## Tick count of the last reading
        self.last_count = self.count
        ## Delta of the last readings
        self.last_delta = self.delta
        ## Updating most recent tick reading
        self.count = self.tim.counter()
        ## Calculating delta between last two readings
        self.delta = self.count - self.last_count
           
        if (self.delta>self.DELTAMAX):
            self.delta = self.delta - self.PER
            
        elif (self.delta<self.DELTAMIN):
            self.delta = self.delta + self.PER
           
        if (self.dir == self.CCW):
            self.delta = - self.delta
           
        self.position = self.position + self.delta
        ## Angular velocity of the motor
        self.velocity = self.delta/(utime.ticks_diff(utime.ticks_us(),self.time))
            
        self.time = utime.ticks_us()
        
    def get_delta(self):
        '''
        @brief This method returns the last delta value.
        @author Alexander Lewis
        '''
        return self.delta
    
    def set_position(self,desired_position):
        '''
        @brief This sets the position to the desired value.
        @author Alexander Lewis
        '''
        self.position = desired_position