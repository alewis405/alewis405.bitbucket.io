# -*- coding: utf-8 -*-
"""
@file ADCALL.py
@brief This module returns the current internal temperature. 
@details The ADCALL script houses the Mcu_t method. This method returns the current internal MCU temperature in degreees Celsius. If the script is run as the main file, it will also save this data to a csv file named mcu_temps.   
@author Alexander Lewis
"""

import pyb

class MCU_temp:
    
    adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
    
    def __init__(self):
        pass
    
    def Mcu_t(self):
        '''
        @brief This method returns the core mcu temperature
        @author Alexander Lewis
        '''          
        mcu_temp = self.adcall.read_core_temp()
        return mcu_temp

if __name__ == "__main__":
    
    mcu = MCU_temp()
    mcu.adcall.read_vref()
    mcu_temperature = mcu.Mcu_t()
    print(mcu_temperature)

    with open ("mcu_temps.csv", "w") as a_file:
        a_file.write ("A line: {:}\r\n".format (mcu_temperature))
        # ...
        print ("The file has by now automatically been closed.")
            
    
    


