var searchData=
[
  ['panel_93',['panel',['../lab9__main_8py.html#af4ce936fb7896fa5f73fcd36f3b966dd',1,'lab9_main']]],
  ['pc13_94',['PC13',['../Lab__2_8py.html#a81398fd8bf2304a3de6fc8b0334f9425',1,'Lab_2']]],
  ['per_95',['PER',['../classencoder_1_1EncoderDriver.html#a2faff0200fded2944f5bc330cd9ea68f',1,'encoder::EncoderDriver']]],
  ['pid_96',['PID',['../classPID_1_1PID.html',1,'PID.PID'],['../classPID_01-_01Copy_1_1PID.html',1,'PID - Copy.PID']]],
  ['pin1_97',['pin1',['../classencoder_1_1EncoderDriver.html#a8663a65a1f1464793fbb8354871afeb5',1,'encoder::EncoderDriver']]],
  ['pin2_98',['pin2',['../classencoder_1_1EncoderDriver.html#ac2abdc547f9336d6b6be6b2cd4a24b35',1,'encoder::EncoderDriver']]],
  ['pin_5fin1_99',['pin_IN1',['../classMotor_1_1MotorDriver.html#a965ba382ac096697dbfb1899d5c29ad9',1,'Motor::MotorDriver']]],
  ['pin_5fin2_100',['pin_IN2',['../classMotor_1_1MotorDriver.html#a289cfacf6d6aeaad3f1c75f83a0c4c90',1,'Motor::MotorDriver']]],
  ['pin_5fnfault_101',['pin_nFault',['../classlab9__main_1_1Controller.html#a6bf784c6073bb48ec9bf729f27c9d0d8',1,'lab9_main.Controller.pin_nFault()'],['../lab9__main_8py.html#a238355d701eb49497c1b83c08e314ebb',1,'lab9_main.pin_nFault()']]],
  ['pin_5fnsleep_102',['pin_nSLEEP',['../classMotor_1_1MotorDriver.html#a6c7a5b672fcf4d0bf2b15743329a1f1d',1,'Motor.MotorDriver.pin_nSLEEP()'],['../lab9__main_8py.html#ae95e308faa689ec8bc547444d4e1913e',1,'lab9_main.pin_nSLEEP()']]],
  ['platform_103',['platform',['../classlab9__main_1_1Controller.html#a6396d5a0d1195801c9585ad9dd95d540',1,'lab9_main::Controller']]],
  ['platform_5ftime_104',['platform_time',['../classlab9__main_1_1Controller.html#a35257b43caecec7857d62096cc454df5',1,'lab9_main::Controller']]],
  ['pos_105',['pos',['../classTouch_1_1Touch.html#afdf8adda368a0dfdcd5020175938d83f',1,'Touch.Touch.pos()'],['../classTouch2_1_1Touch.html#a0700a4a909d20d4d8863a1603ca11caf',1,'Touch2.Touch.pos()']]],
  ['position_106',['position',['../classencoder_1_1EncoderDriver.html#a9cc2828e9445c45bb5d4e753b8052f5a',1,'encoder.EncoderDriver.position()'],['../classtouchpanel_1_1Scanner.html#ae109fab58f71ba69c75381a713eac1fd',1,'touchpanel.Scanner.position()']]]
];
