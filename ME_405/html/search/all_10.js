var searchData=
[
  ['s0_5fhome_113',['S0_Home',['../classLab__3__Computer_1_1User__Interface.html#a7f1431e051213c711871116b8e79ae03',1,'Lab_3_Computer::User_Interface']]],
  ['s1_5fdata_114',['S1_Data',['../classLab__3__Computer_1_1User__Interface.html#a377764f1477cf8ff99fc531a13a5bbe1',1,'Lab_3_Computer::User_Interface']]],
  ['scanner_115',['Scanner',['../classtouchpanel_1_1Scanner.html',1,'touchpanel']]],
  ['scanx_116',['scanX',['../classtouchpanel_1_1Scanner.html#ab2e6e54b8c428aeb7f18971a16ea0edc',1,'touchpanel::Scanner']]],
  ['scany_117',['scanY',['../classtouchpanel_1_1Scanner.html#a3e9c603e28bb188917a126d39e9fcb89',1,'touchpanel::Scanner']]],
  ['scanz_118',['scanZ',['../classtouchpanel_1_1Scanner.html#a0632c8f665c46aa15bc57968777ad512',1,'touchpanel::Scanner']]],
  ['sendchar_119',['sendChar',['../classLab__3__Computer_1_1User__Interface.html#a787ef762000c6841f855e4201b5ca864',1,'Lab_3_Computer::User_Interface']]],
  ['sensor_120',['sensor',['../Touch_8py.html#a1eb1211c86b7bdb55ff9d4d60d1dafa8',1,'Touch']]],
  ['ser_121',['ser',['../classLab__3__Computer_1_1User__Interface.html#aeb9d59523ff4dacf194ea02c83054ecc',1,'Lab_3_Computer::User_Interface']]],
  ['set_5fduty_122',['set_duty',['../classMotor_1_1MotorDriver.html#a420e347ab73de4a9851a5da3434ccfbb',1,'Motor.MotorDriver.set_duty()'],['../classMotor__copy_1_1Driver.html#add5d98adf3e34a4a4ae4c9206ea79eda',1,'Motor_copy.Driver.set_duty()']]],
  ['set_5fposition_123',['set_position',['../classencoder_1_1EncoderDriver.html#a73052ee62e0c78da7030e9d644e4d3fe',1,'encoder.EncoderDriver.set_position()'],['../classMotor__copy_1_1Encoder.html#a1bac3e82f7ddb03f3eff6c8c7236e35f',1,'Motor_copy.Encoder.set_position()']]],
  ['state_124',['State',['../classState__space_1_1State.html',1,'State_space.State'],['../classLab__3__Computer_1_1User__Interface.html#afe78460fd9f8d119df24eda4dd88bb58',1,'Lab_3_Computer.User_Interface.state()']]]
];
