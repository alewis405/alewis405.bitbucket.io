var annotated_dup =
[
    [ "Motor_copy.py", "page8.html#subsection13", null ],
    [ "ADCALL", null, [
      [ "MCU_temp", "classADCALL_1_1MCU__temp.html", "classADCALL_1_1MCU__temp" ]
    ] ],
    [ "ADCCALL", null, [
      [ "MCU_temp", "classADCCALL_1_1MCU__temp.html", "classADCCALL_1_1MCU__temp" ]
    ] ],
    [ "encoder", null, [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "Lab 7", null, [
      [ "Touch", "classLab_017_1_1Touch.html", "classLab_017_1_1Touch" ]
    ] ],
    [ "lab9_main", null, [
      [ "Controller", "classlab9__main_1_1Controller.html", "classlab9__main_1_1Controller" ]
    ] ],
    [ "Lab_2", null, [
      [ "Reaction_Timer", "classLab__2_1_1Reaction__Timer.html", "classLab__2_1_1Reaction__Timer" ]
    ] ],
    [ "Lab_3_Computer", null, [
      [ "User_Interface", "classLab__3__Computer_1_1User__Interface.html", "classLab__3__Computer_1_1User__Interface" ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "Motor", null, [
      [ "MotorDriver", "classMotor_1_1MotorDriver.html", "classMotor_1_1MotorDriver" ]
    ] ],
    [ "Motor_copy", null, [
      [ "Driver", "classMotor__copy_1_1Driver.html", "classMotor__copy_1_1Driver" ],
      [ "Encoder", "classMotor__copy_1_1Encoder.html", "classMotor__copy_1_1Encoder" ]
    ] ],
    [ "PID", null, [
      [ "PID", "classPID_1_1PID.html", "classPID_1_1PID" ]
    ] ],
    [ "PID - Copy", null, [
      [ "PID", "classPID_01-_01Copy_1_1PID.html", "classPID_01-_01Copy_1_1PID" ]
    ] ],
    [ "State_space", null, [
      [ "State", "classState__space_1_1State.html", "classState__space_1_1State" ]
    ] ],
    [ "Touch", null, [
      [ "Touch", "classTouch_1_1Touch.html", "classTouch_1_1Touch" ]
    ] ],
    [ "Touch2", null, [
      [ "Touch", "classTouch2_1_1Touch.html", "classTouch2_1_1Touch" ]
    ] ],
    [ "touchpanel", null, [
      [ "Scanner", "classtouchpanel_1_1Scanner.html", "classtouchpanel_1_1Scanner" ]
    ] ]
];