var classLab__7_1_1Touch =
[
    [ "__init__", "classLab__7_1_1Touch.html#a3c7c5b7ce781336cb103ed36550dfb54", null ],
    [ "pos", "classLab__7_1_1Touch.html#ac72d874f84c01b05ebfec203f0a59234", null ],
    [ "x", "classLab__7_1_1Touch.html#a763005f9cdd5339ba12417da191dc868", null ],
    [ "y", "classLab__7_1_1Touch.html#a6a98310c83115c08549d2099cda39ffa", null ],
    [ "z", "classLab__7_1_1Touch.html#abaee7bbcd4a3caf21db3969ed9303c74", null ],
    [ "height_per_volt", "classLab__7_1_1Touch.html#a4c6a312ba354e113186a316a9908dde5", null ],
    [ "len_per_volt", "classLab__7_1_1Touch.html#a8b88fb2a6f2f8d690ea0a068aef98e61", null ],
    [ "x_center", "classLab__7_1_1Touch.html#a29cf884f31a6ffcb5f2348eac66fce10", null ],
    [ "Xm", "classLab__7_1_1Touch.html#a6d533bc8a08395ed38e20b5466333401", null ],
    [ "Xp", "classLab__7_1_1Touch.html#add9b3f81d3fd5293ea29e25eddaac44e", null ],
    [ "y_center", "classLab__7_1_1Touch.html#a9dd275410ba2c51bcc64c6ddec5a6caa", null ],
    [ "Ym", "classLab__7_1_1Touch.html#a797df787700340233513d93216db7396", null ],
    [ "Yp", "classLab__7_1_1Touch.html#a36d95bf65996d809d14370562d39350e", null ]
];