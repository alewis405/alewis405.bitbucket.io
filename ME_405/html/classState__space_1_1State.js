var classState__space_1_1State =
[
    [ "__init__", "classState__space_1_1State.html#a6c9b11f0f3d9a39b08b5feec9a9f1dda", null ],
    [ "ball", "classState__space_1_1State.html#a133916b27f3905106671693937269b59", null ],
    [ "Prop", "classState__space_1_1State.html#ac958cf85801965261856f34bb09916dc", null ],
    [ "encoder1", "classState__space_1_1State.html#aca6c699b7e0243dd7c02da026faccd64", null ],
    [ "encoder2", "classState__space_1_1State.html#a7a5aec2c3c0ee57f856d563e3b35ff72", null ],
    [ "K1", "classState__space_1_1State.html#ad432cdba10e31a2c61f60661237e5e93", null ],
    [ "K2", "classState__space_1_1State.html#ab48115ea69bea2359822c83a2133350d", null ],
    [ "K3", "classState__space_1_1State.html#a204481c3e45a07e9e61ab3276336adb6", null ],
    [ "K4", "classState__space_1_1State.html#a605a0ab78b4a0aeb5562921f8b0a5d85", null ],
    [ "last_position", "classState__space_1_1State.html#a001797f666181935f311605c373944a6", null ],
    [ "last_time", "classState__space_1_1State.html#ae94d3fb8702dd62ec8981f5c882dfbdb", null ],
    [ "motor1", "classState__space_1_1State.html#a65587e3b6d565eaa1231340faa1e814f", null ],
    [ "motor2", "classState__space_1_1State.html#ac4f7853e3d59d94943bf772d9f564b90", null ],
    [ "position", "classState__space_1_1State.html#a8992b6f66da02c66fd7361f7216333ca", null ],
    [ "sensor", "classState__space_1_1State.html#a1c51a889f2e189b4c1b06bcf0e0a7710", null ],
    [ "x_vel", "classState__space_1_1State.html#a77c9e469ea77bef889eb1ea131c93cde", null ],
    [ "y_vel", "classState__space_1_1State.html#a891ec2d04148f19642fff5246d864f7a", null ]
];