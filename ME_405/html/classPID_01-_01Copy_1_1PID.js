var classPID_01__01Copy_1_1PID =
[
    [ "__init__", "classPID_01-_01Copy_1_1PID.html#ab967be73284b95c709f8f011f23c4fcc", null ],
    [ "ball", "classPID_01-_01Copy_1_1PID.html#a7a15042b177cf0c4174bb529a4ad8204", null ],
    [ "Prop", "classPID_01-_01Copy_1_1PID.html#a9ceda094fb6d6004add744dbec5f3fe1", null ],
    [ "encoder1", "classPID_01-_01Copy_1_1PID.html#a3410377f2e5c587711ae4105937ed9c8", null ],
    [ "encoder2", "classPID_01-_01Copy_1_1PID.html#a6b5793d3120437d463467164a759cd5e", null ],
    [ "Kd", "classPID_01-_01Copy_1_1PID.html#a3714a7e7d7964533b2d5e44ed4c6bf7a", null ],
    [ "Kp", "classPID_01-_01Copy_1_1PID.html#a4134431dba78e797acf95c40e990c917", null ],
    [ "last_position", "classPID_01-_01Copy_1_1PID.html#af8dfe0e129f51e50ac086ccbd3ac96eb", null ],
    [ "last_time", "classPID_01-_01Copy_1_1PID.html#a9c74b8e7c803b725bd74eadaff0c2bcc", null ],
    [ "motor1", "classPID_01-_01Copy_1_1PID.html#a2766895af231bbb33b064dc0e4c62139", null ],
    [ "motor2", "classPID_01-_01Copy_1_1PID.html#a1df884e0c3d21fa326fa38dfbe5f2947", null ],
    [ "position", "classPID_01-_01Copy_1_1PID.html#af0a32fce892267ebebbaa7d3379e6961", null ],
    [ "sensor", "classPID_01-_01Copy_1_1PID.html#a08e21268d9b1d00a8e8b63573b0788bd", null ],
    [ "x_vel", "classPID_01-_01Copy_1_1PID.html#a758a3f5d1a14559580ecfb91864e2f30", null ],
    [ "y_vel", "classPID_01-_01Copy_1_1PID.html#a6b786795f99a9fed00f7d9ab279d9f7a", null ]
];