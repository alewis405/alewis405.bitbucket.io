var classmcp9808_1_1MCP9808 =
[
    [ "__init__", "classmcp9808_1_1MCP9808.html#ac4615d193c6f1df57594cb4c3764b13b", null ],
    [ "celsius", "classmcp9808_1_1MCP9808.html#abec2aa7008fec942521d9fd54e7547b1", null ],
    [ "check", "classmcp9808_1_1MCP9808.html#a7f0be9605522cf82ad16697595154118", null ],
    [ "fahrenheit", "classmcp9808_1_1MCP9808.html#a409291f8c990de853e2bc165c44c3a0c", null ],
    [ "translate_bit", "classmcp9808_1_1MCP9808.html#a4236140ae26de3afd23ca1a803830f20", null ],
    [ "connection", "classmcp9808_1_1MCP9808.html#ac21a568f5f7d66b30d6626dd1b41e586", null ]
];