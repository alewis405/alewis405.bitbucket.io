var classMotor_1_1MotorDriver =
[
    [ "__init__", "classMotor_1_1MotorDriver.html#ac70ea1a739259ab40a452173e9e0b452", null ],
    [ "disable", "classMotor_1_1MotorDriver.html#a9ad4f746ef0e7c217ce790f7ab9260b3", null ],
    [ "enable", "classMotor_1_1MotorDriver.html#a794de1aa1bfc8ff660c75bf7f4ec8038", null ],
    [ "nFault", "classMotor_1_1MotorDriver.html#a1a40b64b4c04d520e500cc7f0f744fa1", null ],
    [ "set_duty", "classMotor_1_1MotorDriver.html#a420e347ab73de4a9851a5da3434ccfbb", null ],
    [ "ch1", "classMotor_1_1MotorDriver.html#a2597a437ed8d312ac90d0d5e2945c1f0", null ],
    [ "ch2", "classMotor_1_1MotorDriver.html#a024781420bb297f34a2745c71025cdcc", null ],
    [ "duty", "classMotor_1_1MotorDriver.html#a5944c862c6ffcc5fc6e59c74e17f4bf8", null ],
    [ "fault_detect", "classMotor_1_1MotorDriver.html#ae93ddc5662edaae65f376df612451bd8", null ],
    [ "pin_IN1", "classMotor_1_1MotorDriver.html#a965ba382ac096697dbfb1899d5c29ad9", null ],
    [ "pin_IN2", "classMotor_1_1MotorDriver.html#a289cfacf6d6aeaad3f1c75f83a0c4c90", null ],
    [ "pin_nSLEEP", "classMotor_1_1MotorDriver.html#a6c7a5b672fcf4d0bf2b15743329a1f1d", null ],
    [ "timer", "classMotor_1_1MotorDriver.html#a93dd3f6608ad30e13b49d9ac609b919d", null ]
];