var classTouch_1_1Touch =
[
    [ "__init__", "classTouch_1_1Touch.html#aaeee40fa8e89036a0d12245809b8cda4", null ],
    [ "pos", "classTouch_1_1Touch.html#afdf8adda368a0dfdcd5020175938d83f", null ],
    [ "x", "classTouch_1_1Touch.html#a2730973862c272015110fde8c5605f5e", null ],
    [ "y", "classTouch_1_1Touch.html#ac2e04ff5edc5a857ecf9e13051af6f85", null ],
    [ "z", "classTouch_1_1Touch.html#a6445e1f9b902a7d5172aa70da7fdad2b", null ],
    [ "height_per_volt", "classTouch_1_1Touch.html#a152e498ae3dd4ccb7f9664d8146b72be", null ],
    [ "len_per_volt", "classTouch_1_1Touch.html#a86a2918c5bb9b5b11e997d7e340f914d", null ],
    [ "x_center", "classTouch_1_1Touch.html#a491926329cb04257e8ee0e0b6a241404", null ],
    [ "Xm", "classTouch_1_1Touch.html#a3ba253d7622e03a445bf9323c0c71908", null ],
    [ "Xp", "classTouch_1_1Touch.html#acb2df1dbe5813b841328e4be78933299", null ],
    [ "y_center", "classTouch_1_1Touch.html#a4d0aa5e1e559648b1edddecb4abb901e", null ],
    [ "Ym", "classTouch_1_1Touch.html#aa5e217e69ad48522e612abd4975c2240", null ],
    [ "Yp", "classTouch_1_1Touch.html#ae6cb1ffd5c18ad2baf55cf7dbc6c97f2", null ]
];