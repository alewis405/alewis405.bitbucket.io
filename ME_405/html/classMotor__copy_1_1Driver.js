var classMotor__copy_1_1Driver =
[
    [ "__init__", "classMotor__copy_1_1Driver.html#a1b5eecc6702c5887c0d6c4514c3695da", null ],
    [ "disable", "classMotor__copy_1_1Driver.html#a81471e51cf2d48e22102f0e9e4f68bc5", null ],
    [ "enable", "classMotor__copy_1_1Driver.html#a0009358db7ede5277eab4e0b09321fc3", null ],
    [ "isr_scan", "classMotor__copy_1_1Driver.html#a74059518e2c60473fa29c77cf557e318", null ],
    [ "set_duty", "classMotor__copy_1_1Driver.html#add5d98adf3e34a4a4ae4c9206ea79eda", null ],
    [ "channel1", "classMotor__copy_1_1Driver.html#a164fe109d82bc8fdc6518828f76cb4ae", null ],
    [ "channel2", "classMotor__copy_1_1Driver.html#abb7526436aaca70e26dceb2dd94f11c0", null ],
    [ "isr1_count", "classMotor__copy_1_1Driver.html#af2a43c3bf787e8827115d3f34d4b0b18", null ],
    [ "isr2_count", "classMotor__copy_1_1Driver.html#aefa2d595e09996116eb01ad9faccef72", null ],
    [ "pwm1", "classMotor__copy_1_1Driver.html#acbc676676e152352f0452c972de46ef4", null ],
    [ "pwm2", "classMotor__copy_1_1Driver.html#ad3ed56230532a392180460cceef0c334", null ],
    [ "sleep_pin", "classMotor__copy_1_1Driver.html#aef03be7851ccb2bc5ed646874faf81ee", null ],
    [ "timer", "classMotor__copy_1_1Driver.html#aaf9d9867222b2e5b9308de053724e06a", null ]
];