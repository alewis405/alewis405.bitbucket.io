var classMotor_1_1Driver =
[
    [ "__init__", "classMotor_1_1Driver.html#afed5f6638b4e2ccc858646b54b92cb5f", null ],
    [ "disable", "classMotor_1_1Driver.html#a2d4ea0008d9db915bd83255c565eb3e6", null ],
    [ "enable", "classMotor_1_1Driver.html#a5d662d14d70abd46addc8457d46417f1", null ],
    [ "set_duty", "classMotor_1_1Driver.html#ad73618649454ef3fb136cda0648ab7cf", null ],
    [ "channel1", "classMotor_1_1Driver.html#abf9fdc650a9105b339b790aabfc01b08", null ],
    [ "channel2", "classMotor_1_1Driver.html#a9e8a0c0e9fd9f175644d6a6bf7502c0a", null ],
    [ "isr1_count", "classMotor_1_1Driver.html#abdace2e5af996f9f9fc6e581055a17e5", null ],
    [ "isr2_count", "classMotor_1_1Driver.html#a02fce74e7919c892f99ae860c49333a1", null ],
    [ "pwm1", "classMotor_1_1Driver.html#a0a9ce8e85165dd596c561b973211340d", null ],
    [ "pwm2", "classMotor_1_1Driver.html#a299e6a39040e17114eb3bbd56c9bdfaa", null ],
    [ "sleep_pin", "classMotor_1_1Driver.html#ae3593578bdefc82063738d84bcec1e16", null ],
    [ "timer", "classMotor_1_1Driver.html#ae60ebdb1c2bc521be0bbc64cfc3acdd1", null ]
];