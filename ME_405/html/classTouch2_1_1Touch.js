var classTouch2_1_1Touch =
[
    [ "__init__", "classTouch2_1_1Touch.html#a2e3c8d43c9822204986a359b91ce4560", null ],
    [ "pos", "classTouch2_1_1Touch.html#a0700a4a909d20d4d8863a1603ca11caf", null ],
    [ "x", "classTouch2_1_1Touch.html#a57c0545c5582880578296db3c73817aa", null ],
    [ "y", "classTouch2_1_1Touch.html#ad2969f332dad91c24847bb3fbfccb65a", null ],
    [ "z", "classTouch2_1_1Touch.html#a7d137f548e7e9526994aef762947cd61", null ],
    [ "height_per_volt", "classTouch2_1_1Touch.html#a8ef3cc05c3732b940c54ec21e6974aa1", null ],
    [ "len_per_volt", "classTouch2_1_1Touch.html#ad24dd6837897617a2b56f9a9d04bda59", null ],
    [ "x_center", "classTouch2_1_1Touch.html#ad5e1c657253eba7e13ab8a23e574c3a1", null ],
    [ "Xm", "classTouch2_1_1Touch.html#a6b90b7243d301df96fc6db03aa8114a1", null ],
    [ "Xp", "classTouch2_1_1Touch.html#a16fb41da9ca288703f1c1f3c61dfe536", null ],
    [ "y_center", "classTouch2_1_1Touch.html#abbf49cda715a0db31699ee45a1adba93", null ],
    [ "Ym", "classTouch2_1_1Touch.html#a8d04293a835ff251a0ec739b759fd08a", null ],
    [ "Yp", "classTouch2_1_1Touch.html#a32921f28816019e9aeab2396946ba39a", null ]
];