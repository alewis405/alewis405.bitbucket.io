var Motor__copy_8py =
[
    [ "Encoder", "classMotor__copy_1_1Encoder.html", "classMotor__copy_1_1Encoder" ],
    [ "Driver", "classMotor__copy_1_1Driver.html", "classMotor__copy_1_1Driver" ],
    [ "isr_1", "Motor__copy_8py.html#a344302fe161db19e3f07f9ac5138b48e", null ],
    [ "isr_2", "Motor__copy_8py.html#a8dbc95795ebfce959520453bb46ce24e", null ],
    [ "encoder1", "Motor__copy_8py.html#a70b9b91737ca4520963ff0dfbee63145", null ],
    [ "encoder2", "Motor__copy_8py.html#a7c94c9773e637e56fab27113fa49b27a", null ],
    [ "extint1", "Motor__copy_8py.html#a77c5cbdfb808cb82b33cbfe0f875ad93", null ],
    [ "extint2", "Motor__copy_8py.html#ade8d91b27b6ba4daa9c06216edf0bdda", null ],
    [ "motor1", "Motor__copy_8py.html#a329705ef33049e4772a4191a10486324", null ],
    [ "motor2", "Motor__copy_8py.html#a6460a8a1b70ec133a8ec2d8c754e570b", null ],
    [ "PB2", "Motor__copy_8py.html#a1c85947fef60d5b6b905c5d6cee99d09", null ],
    [ "PC13", "Motor__copy_8py.html#a61dd69732391d58bc8c2c85dca79ee34", null ],
    [ "sleep_pin", "Motor__copy_8py.html#aa747ff7e0075f6b13cad11eb9a7e0b7f", null ]
];