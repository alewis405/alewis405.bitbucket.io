/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405", "index.html", [
    [ "Lab 1: Vending Machine", "page1.html", [
      [ "Overview", "page1.html#sec1", [
        [ "Lab_1_Vend_FSM.py", "page1.html#subsection1", null ],
        [ "HW_1.py", "page1.html#subsection2", null ],
        [ "State Diagram", "page1.html#subsection3", null ]
      ] ]
    ] ],
    [ "Lab 2: Reaction Time Tester", "page2.html", [
      [ "Overview", "page2.html#sec2", [
        [ "Lab_2.py", "page2.html#subsection4", null ]
      ] ]
    ] ],
    [ "Lab 3: Analog Tracking and Plotting of a Button Pulse", "page3.html", [
      [ "Overview", "page3.html#sec3", [
        [ "main_lab_3.py", "page3.html#subsection5", null ],
        [ "Lab_3_Computer.py", "page3.html#subsection6", null ],
        [ "Analog and Voltage Graphs", "page3.html#subsection7", null ]
      ] ]
    ] ],
    [ "Lab 4: Temperature Tracking", "page4.html", [
      [ "Overview", "page4.html#sec4", [
        [ "ADCALL.py", "page4.html#subsection8", null ],
        [ "mcp9808.py", "page4.html#subsection9", null ],
        [ "Temperature Graph", "page4.html#subsection10", null ],
        [ "main.py", "page4.html#subsection11", null ]
      ] ]
    ] ],
    [ "Lab 5: Balancing System Dynamics", "Lab5.html", [
      [ "Overview", "Lab5.html#sec5", null ],
      [ "Hand Calculations", "Lab5.html#sec6", null ]
    ] ],
    [ "Lab 7: Touch Sensor", "page7.html", [
      [ "Overview", "page7.html#sec7", [
        [ "Touch.py", "page7.html#subsection12", null ]
      ] ]
    ] ],
    [ "Lab 8: Motors, Encoders, and nFault Interrupts", "page8.html", null ],
    [ "Lab 9: Term Project", "page_lab9.html", [
      [ "Term Project Part II Source Code", "page_lab9.html#sec_lab9code", null ],
      [ "Overview", "page_lab9.html#lab9_intro", null ],
      [ "Performance Video", "page_lab9.html#lab9_video", null ],
      [ "File: lab9_main.py", "page_lab9.html#lab9_file1", null ],
      [ "File: motor.py", "page_lab9.html#lab9_file2", null ],
      [ "File: encoder.py", "page_lab9.html#lab9_file3", null ],
      [ "File: touchpanel.py", "page_lab9.html#lab9_file4", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classlab9__main_1_1Controller.html#a6396d5a0d1195801c9585ad9dd95d540"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';