var classPID_1_1PID =
[
    [ "__init__", "classPID_1_1PID.html#ac998d40c7c86bb1bbafc473a360210a5", null ],
    [ "ball", "classPID_1_1PID.html#ab97ca35c11bb1e97f8b8a39e614d3d34", null ],
    [ "Der", "classPID_1_1PID.html#ac9371644d732a84a16b16e80fc87624b", null ],
    [ "Prop", "classPID_1_1PID.html#ae0fd15e9982a45f24fb10dd482b97189", null ],
    [ "encoder1", "classPID_1_1PID.html#a065a23fc620b5e147529c4e69c03170f", null ],
    [ "encoder2", "classPID_1_1PID.html#a3cbc17d1cdf540deb2b97658000abc1a", null ],
    [ "Kdx", "classPID_1_1PID.html#a47a794a7b32206061dc24781a5eb83e0", null ],
    [ "Kdy", "classPID_1_1PID.html#a34a0b2733abf51f06565f28f787380c4", null ],
    [ "Kx", "classPID_1_1PID.html#a32f1f0d52c93cdb939228c5a209a15a1", null ],
    [ "Ky", "classPID_1_1PID.html#a095bb329b8d07bbad95d80fa1e4626da", null ],
    [ "last_position", "classPID_1_1PID.html#a76d2451efc9c9e08bf018436b4961912", null ],
    [ "last_time", "classPID_1_1PID.html#aeb74fa16a3349688f0c9f670f93f356b", null ],
    [ "motor1", "classPID_1_1PID.html#a746fc13216e9db8afe6c772c7814e44b", null ],
    [ "motor2", "classPID_1_1PID.html#a8a962f15c8105285646ba4d196d184bd", null ],
    [ "position", "classPID_1_1PID.html#acbd4d7d9d1d61dbf2e9bf46d6303c06d", null ],
    [ "sensor", "classPID_1_1PID.html#a0f1c3971855c5959d2d9d9e73baa9402", null ],
    [ "x_vel", "classPID_1_1PID.html#ab9cf666e9fb6e064be790f93627c28e3", null ],
    [ "y_vel", "classPID_1_1PID.html#ada666fdfd5b6babaecf476a7d70c9d8a", null ]
];