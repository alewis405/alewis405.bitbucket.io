4444dd1# -*- coding: utf-8 -*-
"""
@file Lab_1_Vend_FSM.py
@brief This module is a virtual finite state machine vending machine.
@details This module contains an FSM to run the virtual vending machine. The buttons are as follows: E - Eject, C - Cuke, P - Popsi, S - Spryte, and D - Dr. Pupper. Money inputs are in the order (0,1,2,3,4,5,6,7) (pennies, nickels, dimes, quarters, ones, fives, tens, twenties). If the vending machine is idle with no money entered or drinks buttons pressed it will prompt a user to enter money and buy a drink. When money is entered, the balance is printed. Then 'E' or 'e' is pressed,  the coins and bills are all ejected and the balance is set back ot zero.
@author: Alexander Lewis
@date: 1/12/2021
@copywrite
"""

import keyboard
import HW_1

class Vending_FSM: 
    '''
    @brief This class creates and runs a vending machine object. 
    @details This vending machine object runs as a FSM. It has 4 drink options as well as an eject button. It accepts pennies, nickels, dimes, quarters, and single, five, ten, and twenty dollar bills. It continually updates the balance while running. 
    @author Alexander Lewis
    @date 1/12/2021
    '''
    ## @brief Home state waits for money or money input.
    S0_Home = 0
    ## @brief If the balance is positive, this is the state. It waits for drink selection or more money.
    S1_Positive_Balance = 1
    ## @brief This is the state while the Cuke is being pushed out of the machine.
    S2_Vending_Cuke = 2
    ## @brief This is the state while the Popsi is being pushed out of the machine.
    S3_Vending_Popsi = 3
    ## @brief This is the state while the Dr. Pupper is being pushed out of the machine.
    S4_Vending_Dr_Pupper = 4
    ## @brief This is the state while the Spryte is being pushed out of the machine.
    S5_Vending_Spryte = 5
    ## @brief This is the state while the balance is ejected.
    S6_Ejecting = 6
    ## @brief In this state, the user will be told they don't have enough money. 
    S7_Error_Prompt = 7
    ## @brief This state will display a sales message if a certain number of runs have passed while remaining on the Home screen. 
    S8_Idle = 8


    def __init__(self):
        '''
        @brief This method initializes the vending machine object.
        @details This initializer sets the initial balance to zero, the number of runs to zero, and the state to S0_Home. It also enables the '0-7' keys and the 'p','P','s','S','d','D','c','C','e','E' keys to be used as inputs. 
        @author Alexander Lewis
        @date 1/12/2021
        '''

        ## @brief Contains the current state
        self.State = self.S0_Home
        ## @brief Contains the number of runs through the FSM
        self.runs = 0 
        ## @brief Tracks the number of runs that have occured all in the home page. 
        self.home_runs = 0
        ## Initialization message
        print("Get a drink! Enter some money and then press the drink you want! ")
        ## This records the last key that was entered. It is always reset to '' immediately after it is used.
        self.last_key = ''
        ## This balance is the current money that is inside the vending machine. 
        self.balance = [0,0,0,0,0,0,0,0]
        self.change = 0
        
        # Tell the keyboard module to respond to these particular keys only
        keyboard.on_release_key("D", callback=self.kb_cb)
        keyboard.on_release_key("C", callback=self.kb_cb)
        keyboard.on_release_key("P", callback=self.kb_cb)
        keyboard.on_release_key("S", callback=self.kb_cb)
        keyboard.on_release_key("d", callback=self.kb_cb)
        keyboard.on_release_key("c", callback=self.kb_cb)
        keyboard.on_release_key("p", callback=self.kb_cb)
        keyboard.on_release_key("s", callback=self.kb_cb)
        keyboard.on_release_key("0", callback=self.kb_cb)
        keyboard.on_release_key("1", callback=self.kb_cb)
        keyboard.on_release_key("2", callback=self.kb_cb)        
        keyboard.on_release_key("3", callback=self.kb_cb)        
        keyboard.on_release_key("4", callback=self.kb_cb)
        keyboard.on_release_key("5", callback=self.kb_cb)
        keyboard.on_release_key("6", callback=self.kb_cb)
        keyboard.on_release_key("7", callback=self.kb_cb)
        keyboard.on_release_key("E", callback=self.kb_cb)
        keyboard.on_release_key("e", callback=self.kb_cb)
        print("Welcome to the vending machine!")

    def kb_cb(self,key):
        '''
        @brief This function updates self.last_key.
        @author Alexander Lewis - Modified ME 405 Resource
        @date 1/12/2021

        '''
        self.last_key = key.name
        
    def transition_To(self,state):
        '''
        @brief This method transitions the FSM state to whatever state is entered
        @author Alexander Lewis
        @date 1/12/2021
        '''
        self.State = state
        
    def run(self):
        '''
        @brief This runs one iteration through the FSM.
        @details This method checks the state, and executes different functions depending on which of the 9 states it is currently in. 
        @author Alexander Lewis
        @date 1/12/2021

        '''

        if (self.State == self.S0_Home):
            
            if self.last_key == '0' or self.last_key == '1' or self.last_key == '2' or self.last_key == '3' or self.last_key == '4' or self.last_key == '5' or self.last_key == '6' or self.last_key == '7':
                money = self.last_key
                self.update_bal(money)
                self.last_key = 0
                self.transition_To(self.S1_Positive_Balance)
                self.home_runs = 0
                
            elif self.last_key == "C" or self.last_key == 'P' or self.last_key == 'D' or self.last_key == 'S' or self.last_key == 's' or self.last_key == 'd' or self.last_key == 'c' or self.last_key == 'p':
                self.transition_To(self.S7_Error_Prompt)
                self.home_runs = 0
                
            elif self.home_runs >= 10000000:
                self.transition_To(self.S8_Idle)
                
            else:
                self.home_runs += 1
                
        elif (self.State == self.S1_Positive_Balance):
            if self.last_key == '0' or self.last_key == '1' or self.last_key == '2' or self.last_key == '3' or self.last_key == '4' or self.last_key == '5' or self.last_key == '6' or self.last_key == '7':
                money = self.last_key
                self.update_bal(money)
                self.last_key = ''
            elif self.last_key == "C" or self.last_key == 'P' or self.last_key == 'D' or self.last_key == 'S' or self.last_key == 's' or self.last_key == 'd' or self.last_key == 'c' or self.last_key == 'p':
                self.pay(self.last_key)
                self.last_key = ''

                self.last_key = ''
            elif self.last_key == "E" or self.last_key == 'e':
                 self.transition_To(self.S6_Ejecting)
                 self.last_key = ''
                 
        elif (self.State == self.S2_Vending_Cuke):
            print("Vending Cuke")
            print("Cuke Vended")
            if self.change == (0,0,0,0,0,0,0,0):
                print('Thank you for your purchase')
                self.transition_To(self.S0_Home)
            else:
                self.balance = self.change
                self.transition_To(self.S1_Positive_Balance)
                print('Would you like another drink?')
                print('If you would like your change, press E')
                
        elif (self.State == self.S3_Vending_Popsi):
            print("Vending Popsi")
            print("Popsi Vended")
            print("")
            if self.change == (0,0,0,0,0,0,0,0):
                print('Thank you for your purchase')
                self.transition_To(self.S0_Home)
            else:
                self.balance = self.change
                self.transition_To(self.S1_Positive_Balance)
                print('Would you like another drink?')
                print('If you would like your change, press E')
                
        elif (self.State == self.S4_Vending_Dr_Pupper):
            print("Vending Dr Pupper")
            print("Dr Pupper Vended")
            if self.change == (0,0,0,0,0,0,0,0):
                print('Thank you for your purchase')
                self.transition_To(self.S0_Home)
            else:
                self.balance = self.change
                self.transition_To(self.S1_Positive_Balance)
                print('Would you like another drink?')
                print('If you would like your change, press E')
                
        elif (self.State == self.S5_Vending_Spryte):
            print("Vending Spryte")
            print("Spryte Vended")
            if self.change == (0,0,0,0,0,0,0,0):
                print('Thank you for your purchase')
                self.transition_To(self.S0_Home)
            else:
                self.balance = self.change
                self.transition_To(self.S1_Positive_Balance)
                print('Would you like another drink?')
                print('If you would like your change, press E')
                
        elif (self.State == self.S6_Ejecting):
            print("Ejecting")  
            self.update_bal('e')
            print("Change ejected!")
            self.transition_To(self.S0_Home)
        elif (self.State == self.S7_Error_Prompt):
            print("Please enter money and then select your drink again!")
            
            if all([ n == 0 for n in self.balance ]) :
                self.transition_To(self.S0_Home) 
            else:
                self.transition_To(self.S1_Positive_Balance)
            self.last_key = ''
        elif (self.State == self.S8_Idle):
            print("Hello! Enter money and choose your drink!")
            self.transition_To(self.S0_Home)
            self.home_runs = 0
            pass
        
    
    def update_bal(self,money):
        '''
        @brief This method updates the balance every time a coin or bill is added, or when the money is getting ejected. 
        @author Alexander Lewis
        @date 1/12/2021

        '''
        money = str(money)
        self.balance = list(self.balance)
        if money == "0":
            self.balance[0] += 1
            print('hello')
        elif money == "1":
            self.balance[1] += 1
        elif money == "2":   
            self.balance[2] += 1
        elif money == "3":
            self.balance[3]+= 1
        elif money == "4":
            self.balance[4] += 1
        elif money == "5":
            self.balance[5] += 1
        elif money == "6":
            self.balance[6] += 1
        elif money == "7":
            self.balance[7] += 1
        elif money == "E":
            self.balance = [0,0,0,0,0,0,0,0]
        elif money == "e":
            self.balance = [0,0,0,0,0,0,0,0]
            
        else:
            pass
        print("Balance: ")
        print(self.balance)
    
    
    def get_bal(self):
        '''
        @brief This method changes the current balance into a tuple, in order to be used with the change calculating module.
        @author Alexander Lewis
        @date 1/16/2021
        '''
        b = tuple(self.balance)
        return b
    
    def pay(self,Drink):
        '''
        @brief This method attempts to purchase a drink with whatever the current balance is.
        @details This method will subtract the drink cost from the balance and transition the state to the vending for that specific drink. If the balance is insufficient for the cost of the drink, the state will transition to error. 
        @author Alexander Lewis
        @date 1/16/2021
        '''
        
        if Drink == 'P' or Drink == 'p':
            bal = self.get_bal()
            self.change = HW_1.getChange(1.20,bal)
            if self.change == (0,0,0,0,0,0,0,1000):  
                self.transition_To(self.S7_Error_Prompt)
            else: 
                self.transition_To(self.S3_Vending_Popsi)
                
                
        elif Drink == 'C' or Drink == 'c':
            bal = self.get_bal()
            self.change = HW_1.getChange(1.00,bal)
            if self.change == (0,0,0,0,0,0,0,1000):  
                self.transition_To(self.S7_Error_Prompt)
            else: 
                self.transition_To(self.S2_Vending_Cuke)             
                
                
        elif Drink == 'D' or Drink == 'd':
            bal = self.get_bal()
            self.change = HW_1.getChange(1.10,bal)
            if self.change == (0,0,0,0,0,0,0,1000):  
                self.transition_To(self.S7_Error_Prompt)
            else: 
                self.transition_To(self.S4_Vending_Dr_Pupper)
                
                            
        elif Drink == 'S' or Drink == 's':
            bal = self.get_bal()
            self.change = HW_1.getChange(.85,bal)
            if self.change == (0,0,0,0,0,0,0,1000):  
                self.transition_To(self.S7_Error_Prompt)
            else: 
                self.transition_To(self.S5_Vending_Spryte)
        else:
            pass
        
                           
        
        self.last_key = ''
        
            
    
    









if __name__ =="__main__":
    

    machine = Vending_FSM()
    while True: 
        machine.run()

