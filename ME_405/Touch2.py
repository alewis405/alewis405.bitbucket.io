# -*- coding: utf-8 -*-
"""
@file Touch.py
@brief This script contains the Touch class which contains the required methods to return the touch sensor positions. 
@details This script contains the pos() method and supporting methods. pos() returns a tuple containing the x, y, and z (on/off) positions. When run as the main file, this script returns the average time taken to run the pos() method out of 1000 tests.   
@author Alexander Lewis
@date 3/7/2021
"""
from pyb import ADC
from pyb import Pin
import utime

class Touch:
    '''
    @brief This class creates a touch sensor object. 
    @author Alexander Lewis
    @date 3/7/2021
    '''
    def __init__(self,height,length):
        '''
        @brief This initializer specifies the details of the touch sensor dimensions.
        @author Alexander Lewis
        @date 3/7/2021
        '''
        self.len_per_volt = (length/2)/(3700-2076)
        self.x_center = 2076
        self.height_per_volt = (height/2)/(3000-1855)
        self.y_center = 1855
        self.Yp = Pin(Pin.board.PA7)
        self.Ym = Pin(Pin.board.PA6)
        self.Xp = Pin(Pin.board.PA1)
        self.Xm = Pin(Pin.board.PA0)
    
    def x(self):
        '''
        @brief This method returns the position in the x direction.
        @author Alexander Lewis
        @date 3/7/2021
        '''
        self.Yp.init(mode = Pin.IN)
        self.Ym.init(mode = Pin.ANALOG)
        self.Xp.init(mode = Pin.OUT_PP)
        self.Xm.init(mode = Pin.OUT_PP)
        adc = ADC(self.Ym)
        
        self.Yp.low()
        self.Xp.high()
        self.Xm.low()
        volts = adc.read()
        pos = volts * self.len_per_volt - self.x_center * self.len_per_volt
        return pos
        
        
    
    def y(self):
        '''
        @brief This method returns the position in the y direction.
        @author Alexander Lewis
        @date 3/7/2021
        '''
        self.Ym.init(mode = Pin.OUT_PP)
        self.Xp.init(mode = Pin.IN)
        self.Xm.init(mode = Pin.ANALOG)
        self.Yp.init(mode = Pin.OUT_PP)
        adc = ADC(self.Xm)
        
        self.Xp.low()
        self.Ym.low()
        self.Yp.high()
        volts = adc.read()
        pos = volts * self.height_per_volt - self.y_center * self.height_per_volt
        return pos
       
    
    def z(self):
        '''
        @brief This method returns 0 or 1 to show if any contact with the sensor is detected. 
        @author Alexander Lewis
        @date 3/7/2021
        '''
        self.Yp.init(mode = Pin.OUT_PP)
        self.Xp.init(mode = Pin.ANALOG)
        self.Ym.init(mode = Pin.ANALOG)
        self.Xm.init(mode = Pin.OUT_PP)
        adc = ADC(self.Yp)
        if adc.read()<5:
            return 1
        else:
            return 0
        
    def pos(self):
        '''
        @brief This method returns the tuple containing x, y, and z. 
        @author Alexander Lewis
        @date 3/7/2021
        '''
        x = self.x()
        z = self.z()
        y = self.y()
        
        position = (x, y, z)
        return position
    

        
        
        
        
        
if __name__ == "__main__":
    ## This part of the script tests and returns the average speed of the pos() function. 
    sensor = Touch(100,176)
    test_time_start = utime.ticks_us()
    last_time = utime.ticks_us()
    intervals = 0
    time = utime.ticks_us()
    sensor.pos()
    interval = utime.ticks_diff(time, last_time)
    last_time = time
    for n in range(1000):
        time = utime.ticks_us()
        sensor.pos()
        interval = utime.ticks_diff(time, last_time)
        last_time = time
        intervals += interval
    average_speed = intervals/1000
    print(average_speed)
    while True:
        print(sensor.pos())
        utime.sleep(.1)


    