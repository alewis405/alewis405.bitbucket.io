# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 21:05:09 2021

@author: Alexander Lewis
"""
import pyb 



class Touch:
    
    def __init__(self):
        
        self.Yp = pyb.Pin(pyb.Pin.board.PA7)
        self.Ym = pyb.Pin(pyb.Pin.board.PA6)
        self.Xp = pyb.Pin(pyb.Pin.board.PA1)
        self.Xm = pyb.Pin(pyb.Pin.board.PA0)
        pass
    
    def x(self):
        pass
    
    def y(self):
        self.Yp.init(mode = pyb.Pin.OUT_PP)
        self.Ym.init(mode = pyb.Pin.OUT_PP)
        self.Xp.init(mode = pyb.Pin.IN)
        self.Xm.init(mode = pyb.Pin.ANALOG)
        pass
    
    def z(self):
        pass
    