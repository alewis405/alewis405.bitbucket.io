# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 21:16:58 2021

@author: Alexander Lewis
"""

import pyb
import utime
isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine

# After some events have happened on the pin...
while True:
    print (isr_count)
    utime.sleep_ms(100)