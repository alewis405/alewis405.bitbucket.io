# -*- coding: utf-8 -*-
"""
@file main_page.py
@mainpage
@author Alexander Lewis


This portfolio showcases and provides documentation for all the labs I have completed during ME_405. 
During this class I learned to use temperature sensors, state-space control, analog ports on the nucleo,
a touch sensor, and interrupts including fault interrupts. 


@page page1 Lab 1: Vending Machine
  @section sec1 Overview
  In this lab, I created a virtual vending machine. It keeps track of how much change it holds. It responds to user requests for different priced drinks differently depending on how much money is in the machine. It contains an eject function and will flash an advertisement if no actions are taken for a while.The virtual machine uses the get_Change() method found in HW_1.py.  
  This page contains the subsections \ref subsection1 ,  \ref subsection2 , and \ref subsection3
  @subsection subsection1 Lab_1_Vend_FSM.py
  
  Documentation Link: https://alewis405.bitbucket.io/ME_405/html/Lab__1__Vend__FSM_8py.html
  
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/Lab_1_Vend_FSM.py
  
  @subsection subsection2 HW_1.py
  Documentation link: https://alewis405.bitbucket.io/ME_405/html/HW__1_8py.html
  
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/HW_1.py

  @subsection subsection3 State Diagram
  @image html 405_HW_00.jpg



@page page2 Lab 2: Reaction Time Tester
  @section sec2 Overview
  In Lab 2, I made Lab_2.py, a reaction time tester using the Nucleo Microcontroller. This involved using interrupts and timing. The program asks a user to get ready. Then it waits a random amount of time somewhere from 2-3 seconds. Then the nucleo green A15 LED turns on and the time it takes for the user to press the blue C13 button is tracked and displayed. 
    
  @subsection subsection4 Lab_2.py
    
  Documentation link: https://alewis405.bitbucket.io/ME_405/html/Lab__2_8py.html
    
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/Lab_2.py 

    
   
@page page3 Lab 3: Analog Tracking and Plotting of a Button Pulse
  @section sec3 Overview
  This lab has deepened my understanding of the analog abilities of the Nucleo microcontroller. The nucleo is constantly recording batches of data and checking if they are good. A 'good' batch of  data is the pulse that occurs when the C13 blue button is pressed. When a good batch is found, it passes that data to the computer over the UART to be plotted. It took me a little extra time, but I'm happy that in the end it works. It can take a couple button pressing attempts, but this is to be expected. One thing I learned was that the data must not be in string form when it is plotted. 
  @subsection subsection5 main_lab_3.py
  Documentation link: https://alewis405.bitbucket.io/ME_405/html/main__lab__3_8py.html
    
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/main_lab_3.py 
  
  @subsection subsection6 Lab_3_Computer.py
  Documentation Link: https://alewis405.bitbucket.io/ME_405/html/Lab__3__Computer_8py.html
    
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/Lab_3_Computer.py
    
  @subsection subsection7 Analog and Voltage Graphs
  These are the analog and voltage graphs I generated:
   
  @image html 405_LAB_03_Analog.png
  @image html 405_LAB_03_Volts.png
    


 




@page page4 Lab 4: Temperature Tracking
  @section sec4 Overview
  This lab used the mcp9808 sensor and the nucleo internal temperature sensor. The sensor and MCU temperature are both reading correctly. There were some issues gathering data for the full 8 hours.
  
  This page contains the subsections \ref subsection8 ,  \ref subsection9 , \ref subsection10 , \ref subsection11
    
  @subsection subsection8 ADCALL.py
  Documentation Link: https://alewis405.bitbucket.io/ME_405/html/ADCALL_8py.html
    
  Repository Code Link: https://alewis405.bitbucket.io/ME_405/html/ADCALL_8py.html
  @subsection subsection9 mcp9808.py
  Documentation Link: https://alewis405.bitbucket.io/ME_405/html/mcp9808_8py.html
    
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/mcp9808.py
    
  @subsection subsection10 Temperature Graph
  @image html 4.png  
  @image html Lab_4.JPG
  
  @subsection subsection11 main.py
  Documentation Link: https://alewis405.bitbucket.io/ME_405/html/main_8py.html
  
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/main.py

@page Lab5 Lab 5: Balancing System Dynamics
  @section sec5 Overview
  Lab 5 was about calculating the dynamics of the platform system.
  @section sec6 Hand Calculations
  First, a diagram is drawn to show all the dimensions of the system. Then the simplified vector loop is drawn. 
  @image html A.PNG
  @image html B.PNG
  Using the vector loop drawn, the 4 vector loop equations are made. Then they are combined into a single equation with an i and k component. Using substitution, the equations are used to find f(motor position) = platform angle. 
  @image html C.PNG
  @image html D.PNG
  @image html E.PNG
  @image html F.PNG
  Next, Kinetic and Free Body diagrams are used to solve for the linear acceleration of the ball and the angular acceleration of the platform. 
  @image html G.PNG
  @image html H.PNG
  @image html I.PNG
  @image html J.PNG
  @image html K1.PNG

    
@page page7 Lab 7: Touch Sensor
  @section sec7 Overview
  This lab set up the touch sensor. The Touch.py script contains the Touch class with the pos() method. It uses supporting methods to return an (x,y,z) tuple. When run as the main file, it runs 1000 tests to determine the average speed of the pos() method. The average speed is roughly 962 micro-seconds.
  @subsection subsection12 Touch.py
  Documentation link: https://alewis405.bitbucket.io/ME_405/html/Lab__7_8py.html
  
  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/main.py

  

@page page8 Lab 8: Motors, Encoders, and nFault Interrupts
  This lab created a script containing a Driver and Encoder class. It also contains code to scan for ISR interupts caused by motor problems. 

  This page contains the subsections \ref subsection13

  @subsection subsection13 Motor_copy.py
  Documentation link: https://alewis405.bitbucket.io/ME_405/html/Motor__copy_8py.html

  Repository Code Link: https://bitbucket.org/alewis405/alewis405.bitbucket.io/src/master/ME_405/Motor_copy.py
  
@page page_lab9 Lab 9: Term Project
  @date March 17, 2021
  @author Collaboration between Michael Yiu & Alex Lewis

  @section sec_lab9code Term Project Part II Source Code
  The source code can be found at:

  https://bitbucket.org/mwyiu/term-project/src/master/lab9_main.py

  https://bitbucket.org/mwyiu/term-project/src/master/motor.py

  https://bitbucket.org/mwyiu/term-project/src/master/encoder.py

  https://bitbucket.org/mwyiu/term-project/src/master/touchpanel.py

  @section lab9_intro Overview
  This lab is the culmination of the last four labs. In this lab, the student will
  integrate the hardware, motor and encoder drivers, and system model altogether.
  The ideal goal is to have the rubber ball balanced on the platform but trial and
  error will have to be conducted to determine the best controller gain values. The
  video seen below displays the performance of the final system.

  @section lab9_video Performance Video
  \htmlonly
  <iframe width="560" height="315" src="https://www.youtube.com/embed/YlG8B2QPm_M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  \endhtmlonly
  https://youtu.be/YlG8B2QPm_M

  As seen in this YouTube video, Michael and I had limited success on our ball
  balancing platform. Shown is my platform shifting the ball back and forth 
  towards the center but it is eventually thrown off. We'd like to note that the 
  x-axis motor was not functioning properly due to a loose set screw on the gear
  connected to the motor shaft.

  @section lab9_file1 File: lab9_main.py
  lab9_main.py contains the controller class and code to initialize and run the 
  controller. The controller class houses four functions: an initializer, 
  duty_cycle(), ball_update(), and platform(). duty_cycle() uses the four state 
  variables to calculate the duty cycle to be passed to the motors. It sets the 
  duty cycle for both motors each time it is called. ball_update() tracks the 
  position and velocity of the ball using the touch sensor every time it is called.
  platform() tracks the angle an angular velocity of the platform using information 
  from the encoders on each motor. lab9_main.py allows for different gain values 
  to be tested on the controller. 

  @section lab9_file2 File: motor.py
  This file is a slightly modified version of the motor file created in Lab 8. The
  external interrupt was removed and was instead placed in the lab9_main.py file.
  This was due to the main file having the Controller class that controlled both
  motors, so it became much easier to have it control the nFault pins rather than
  the motors. The nFault pin is still on Pin B2, which is active low; Pin B2 is
  typically triggered by an over-current solution. This class includes three 
  main functions: enable(), turning on the motors, disable(), turning off the
  motors, and set_duty(), setting the duty cycle of the motor to a specific value.

  @section lab9_file3 File: encoder.py
  encoder.py contains the EncoderDriver class. This class allows the user to track
  the position of an encoder. This class contains four methods: an initializer, 
  update(), get_delta(), and set_position(). The update() method calculates a new
  delta value from the encoder it is tracking and adds it to a running position 
  tally. This occurs every time the update() method is called. get_delta() returns
  the current delta value. set_position() allows the user to set the current encoder
  position to any value. 

  @section lab9_file4 File: touchpanel.py
  This file is a slightly modified version of the touchpanel file used in Lab 6.
  However, for all intents and purposes, it's essentially the same file. The 
  Scanner class scans the position of contact on the touch panel as an x, y, and 
  z position. There is a four microsecond delay to wait for the settling time and
  all three positions are measured in under 1,500 microseconds. These position
  values are stored in a postion variable in the format of a tuple, rather than
  returned, to improve optimization.

"""

