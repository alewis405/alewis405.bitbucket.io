# -*- coding: utf-8 -*-
"""
@file main_lab_3.py
@brief This script is constantly gathering data and evaluating it. 
@detail This script is continually creating arrays of 1000 datapoints. Then it evaluates based on the minimum and maximum array values whether or not it is a good dataset. 
@author Alexander Lewis
"""

import pyb
from pyb import UART   
import array 
## Set up UART connection
myuart = UART(2)
PC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.OUT_PP)
PC13.low()

## Set up ADC analog pin
adc = pyb.ADC(pyb.Pin.board.A1)

## Timer 6 set at 200 kHz
tim = pyb.Timer(6, freq=200000) 

## Creates the buffer array
buf = array.array ('H', (0 for index in range (1000)))



isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1


extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine


time = []
for i in range(1000):
    time.append(i)

while True:    
    adc.read_timed(buf, tim)         
    ## This line checks to make sure the data is good, and that it is rising data not falling data.
    if (max(buf) > 4000) and min(buf) == 0 and buf[0] == 0: 
        myuart.write('C')
        for n in buf:
            myuart.write(str(n) + ',')
            
            

            
