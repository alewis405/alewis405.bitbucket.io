# -*- coding: utf-8 -*-
"""
@file Motor_copy.py
@brief This script contains both the encoder and driver classes for the motor. 
@author Alexander Lewis
"""
import pyb
import utime

class Encoder:
    '''
    @brief This class tracks the position of the motor. 
    @author Alexander Lewis
    '''
    def __init__(self, pin1, pin2, timer):
        '''
        @brief This initializer takes two pins and the timer required to run the encoder.
        @author Alexander Lewis
        '''
        self.timer = timer
        self.pin1 = pin1
        self.pin2 = pin2

        self.tim = pyb.Timer(timer)
        
        self.tim.init(prescaler = 0,period =0xFFFF)
        
        self.tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        ## Initialize count, delta, and position at zero
        self.count = 0
        self.delta = 0
        self.position = 0 
        self.last_count = 0
        self.last_delta = 0
        
        self.DELTAMAX = 0xFFFF/2
        self.DELTAMIN = -0xFFFF/2    

        self.dir = 1
        self.CCW = 0 
        self.PER = 0xFFFF
        self.start = utime.ticks_ms()
        self.end = self.start
        
        self.time = utime.ticks_us()
    
    def update(self):
        '''
        @brief Every time this method is run it updates the position of the motor. 
        @author Alexander Lewis
        '''
        self.last_count = self.count
        self.last_delta = self.delta
        self.count = self.tim.counter()
        self.delta = self.count - self.last_count
           
        if (self.delta>self.DELTAMAX):
            self.delta = self.delta - self.PER
            
        elif (self.delta<self.DELTAMIN):
            self.delta = self.delta + self.PER
           
        if (self.dir == self.CCW):
            self.delta = - self.delta
           
        self.position = self.position + self.delta
        self.velocity = self.delta/(utime.ticks_diff(utime.ticks_us(),self.time))
            
        self.time = utime.ticks_us()
        
    def get_delta(self):
        '''
        @brief This method returns the last delta value.
        @author Alexander Lewis
        '''
        return self.delta
    
    def set_position(self,desired_position):
        '''
        @brief This sets the position to the desired value.
        @author Alexander Lewis
        '''
        self.position = desired_position
        

class Driver:
    ''' 
    @brief This class enables, disables, and sets the duty percentage and direction for the motor.
    @author Alexander Lewis
    '''
    def __init__(self,pin1,pin2,sleep_pin,timer,ch1,ch2): 
        '''
        @brief This initializer takes the pins, channels, timer, and sleep pin required to run the motor. 
        @author Alexander Lewis
        '''
        self.sleep_pin = sleep_pin #pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
        self.sleep_pin.init(mode = pyb.Pin.OUT_PP)
        self.pwm1 = pin1           #pyb.Pin(pyb.Pin.cpu.B0)
        self.pwm2 = pin2           #pyb.Pin(pyb.Pin.cpu.B1)
        self.timer = pyb.Timer(timer,freq = 20000)
        self.channel1 = self.timer.channel(ch1,pyb.Timer.PWM,pin=self.pwm1)
        self.channel2 = self.timer.channel(ch2,pyb.Timer.PWM,pin = self.pwm2)
        
        ## sets high pin to low until motor should be on
        self.sleep_pin.low()

        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
        self.isr1_count = 0
        self.isr2_count = 0
    
    def enable(self):
        '''
        @brief This enables both motors by turning the sleep pin to high. 
        @author Alexander Lewis
        '''
        self.sleep_pin.high()
        
    def disable(self):
        '''
        @brief This disables both motors by turning the sleep pin to low. 
        @author Alexander Lewis
        '''
        self.sleep_pin.low()
         
    def set_duty(self, percentage,direction):
        '''
        @brief This sets the duty cycle to the entered percentage and sets the direction based on 1 or 0.
        @author Alexander Lewis
        '''
        if direction == 1:
            self.channel1.pulse_width_percent(percentage)
            self.channel2.pulse_width_percent(0)
        else:
            self.channel2.pulse_width_percent(percentage)
            self.channel1.pulse_width_percent(0) 

     
    def isr_scan(self):
          '''
          @brief This method scans for ISR interupts.
          @details This method first scans for the fault pin being low. Then it waits until the blue button is pressed before things return to normal. 
          @author Alexander Lewis
          '''
          if self.isr1_count <= 1:
              self.isr1_count = 0
              self.disable()
              self.isr2_count = 0
              print("MOTOR ERROR. PRESS BLUE BUTTON TO CONTINUE")
              while self.isr2_count == 0:
                  pass 
              isr_2 = 0
              self.enable()
          else:
              pass
        
            
if __name__ == "__main__":

    encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4)
    encoder2 = Encoder(pyb.Pin.cpu.C6,pyb.Pin.cpu.C7,8)
    sleep_pin = pyb.Pin(pyb.Pin.cpu.A15)
    motor1 = Driver(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,sleep_pin,3,1,2)
    motor2 = Driver(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,sleep_pin,3,3,4)
    motor2.disable()
    motor1.disable()
    
    def isr_1 (which_pin):        # Create an interrupt service routine
          '''
          @brief This method tracks isr_1 - fault pin
          @author Alexander Lewis
          '''
          global isr1_count
          isr1_count += 1

    def isr_2 (which_pin):        # Create an interrupt service routine
         '''
         @brief This method tracks isr_2 - Nucleo Blue Button
         @author Alexander Lewis
         '''
         global isr2_count
         isr2_count += 1
    
    PB2 = pyb.Pin(pyb.Pin.board.PB2, mode = pyb.Pin.IN)
    PC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)
    
    motor1.enable()
    extint1 = pyb.ExtInt (pyb.Pin.board.PB2,   # Which pin
              pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
              pyb.Pin.PULL_UP, 
              isr_1) 
    
    extint2 = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
              pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
              pyb.Pin.PULL_UP,             # Activate pullup resistor
              isr_2) 
    


    
        
    
    

        
        
        
        
        