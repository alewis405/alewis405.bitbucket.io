# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 09:15:14 2021

@author: Alexander Lewis
"""

data = ['e','f','g','h']

with open ("temp_Data.txt", "w") as a_file:
    for data_point in data:
        a_file.write(data_point + "\r\n")
        
        # ...
print ("The file has by now automatically been closed.")