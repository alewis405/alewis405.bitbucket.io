# -*- coding: utf-8 -*-
"""
@file HW_1.py
@brief This file contains the getChange method used to calculate the change or insufficient funds from a drink button press. 

@author: Alexander Lewis
"""

def getChange(price, payment):
    '''
    @brief This method uses the price and payment given to calculate the correct amount to be paid back.
    @details The price is a number representing the price of an item. The payment is in the format (pennies, nickles, dimes, quarters, ones, fives, tens, and twenties).
    @author Alexander Lewis
    '''
    Paid = payment[0]*.01 + payment[1]*.05 + payment[2]*.1 + payment[3]*.25 + payment[4]+ payment[5]*5 + payment[6]*10 + payment[7]*20
    Difference = round(Paid - price, 2)     
    if (Difference < 0): 
        ## This assumes no one will put $20,000 in a machine that gives change in pennies. 1000 in the 20 place will signal insufficient funds.
        change = (0,0,0,0,0,0,0,1000)
        return change
    elif (Difference == 0):
        change = (0,0,0,0,0,0,0,0)
        return change
    else: 
        change = [0,0,0,0,0,0,0,0]
        change[7] = int(Difference // 20)
        Difference = Difference - change[7]*20
        change[6] = int(Difference // 10)
        Difference = Difference - change[6]*10
        change[5] = int(Difference // 5)
        Difference = Difference - change[5]*5
        change[4] = int(Difference // 1)
        Difference = Difference - change[4]*1
        change[3] = int(Difference // .25)
        Difference = Difference - change[3]*.25
        change[2] = int(Difference // .1)
        Difference = Difference - change[2]*.1
        change[1] = int(Difference // .05)
        Difference = Difference - change[1]*.05
        change[0] = int(Difference //.01)
        Difference = Difference - change[0]*.01
        change = (change[0],change[1],change[2],change[3],change[4],change[5],change[6],change[7])
        return change

if __name__ =="__main__":
    
    price = 55.29
    payment = (0,0,0,0,0,0,0,4)

    change = getChange(price, payment)
    print(change)