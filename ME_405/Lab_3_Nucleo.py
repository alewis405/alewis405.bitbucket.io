# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 09:40:32 2021

@author: Alexander Lewis
"""
import pyb
from pyb import UART
import utime

myuart = UART(2)

pin = pyb.Pin(pyb.Pin.board.PA4, mode=pyb.Pin.IN)
adc = pyb.ADC(pin) 




start = utime.time_ns()
current = utime.time_ns()

while True:
    
    if (current - start > 100): 
        voltage = adc.read()
        myuart.write((current,))
        
    