# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 09:13:59 2021

@author: Alexander Lewis
"""
import pyb
from Motor import Encoder
from Motor import Driver
from Touch2 import Touch
import utime


class PID: 
    
    def __init__(self,encoder1, encoder2, motor1, motor2,touch_sensor,Kx,Ky,Kdx,Kdy):
        self.encoder1 = encoder1
        self.encoder2 = encoder2
        self.motor1 = motor1
        self.motor2 = motor2
        self.sensor = touch_sensor
        self.last_position = (0,0,0)
        self.position = self.sensor.pos()
        
        self.last_time = utime.ticks_us()
        self.Kx = Kx
        self.Ky = Ky
        self.Kdx = Kdx
        self.Kdy = Kdy
        self.encoder1.update()
        self.encoder2.update()
        self.x_vel = 0
        self.y_vel = 0
        
    def ball(self):
        self.last_position = self.position
        self.position = self.sensor.pos()
        self.encoder1.update()
        self.encoder2.update()
        
        if self.position[2] == 1: 
            time_delta = utime.ticks_diff(utime.ticks_us(),self.last_time)
            self.x_vel = (self.position[0] - self.last_position[0])/(time_delta/100000)
            self.y_vel = (self.position[1] - self.last_position[1])/(time_delta/100000)
            self.last_time = utime.ticks_us()

        

    
    
    def Prop(self):
        self.motor1.enable()
       # self.motor1.isr_scan()

        ## Error is in percentage
        x_er = (self.position[0]/88)*100
        
        y_er = (self.position[1]/50)*100
      
        if self.position[2] == 0:
            self.motor1.disable()
            
            
        else:
            self.motor1.enable()
            if x_er > 0:
                duty = 0
                prop = self.Kx*x_er
                der = abs(self.x_vel*self.Kdx)
                if self.x_vel > 0: 
                    duty = prop + der 
                elif self.x_vel < 0:
                    duty = abs(prop - der)
                if duty > 100:
                    duty = 100
                

                self.motor2.set_duty(duty,0)
                

                
            else:
                duty = 0
                prop = abs(self.Kx*x_er)
                der = abs(self.x_vel)*self.Kdx
                if self.x_vel > 0: 
                    duty = abs(prop - der)
                elif self.x_vel < 0:
                    duty = prop + der
                if duty > 100:
                    duty = 100
                                    
                self.motor2.set_duty(duty,1)


            if y_er >0:  
                prop = self.Ky*y_er
                der = abs(self.y_vel)*self.Kdy
                if self.y_vel > 0: 
                    duty = prop + der 
                elif self.y_vel < 0:
                    duty = abs(prop - der)
                duty = prop + der
                if duty > 70:
                    duty = 70

                self.motor1.set_duty(duty,0)


            else:
                prop = abs(self.Ky*y_er)
                der = abs(self.y_vel)*self.Kdy
                if self.x_vel > 0: 
                      duty = abs(prop - der)
                elif self.x_vel < 0:
                    duty = prop + der
                duty = prop
                if duty > 70:
                    duty = 70

                self.motor1.set_duty(duty,1)
            
    def Der(self):
            if self.x_vel > 0:
                duty = self.x_vel*self.Kd
                self.motor2.set_duty(duty,0)
                
            elif self.x_vel <= 0:
                duty = self.x_vel*self.Kd
                self.motor2.set_duty(duty,1)
                
            elif self.y_vel > 0:
                duty = self.x_vel*self.Kd
                self.motor1.set_duty(duty,0)
                
            elif self.y_vel <= 0:
                duty = self.x_vel*self.Kd
                self.motor1.set_duty(duty,1)
            utime.sleep(.01)
        
            
        
        
        
encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4)
encoder2 = Encoder(pyb.Pin.cpu.C6,pyb.Pin.cpu.C7,8)
sleep_pin = pyb.Pin(pyb.Pin.cpu.A15)
motor1 = Driver(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,sleep_pin,3,1,2)
motor2 = Driver(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,sleep_pin,3,3,4)
motor2.enable()
motor1.enable()
sensor = Touch(100,176)
    
PB2 = pyb.Pin(pyb.Pin.board.PB2, mode = pyb.Pin.IN)
PC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

# extint1 = pyb.ExtInt (pyb.Pin.board.PB2,   # Which pin
#          pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#          pyb.Pin.PULL_UP,             # Activate pullup resistor
#          motor1.isr_1) 

# extint2 = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
#          pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#          pyb.Pin.PULL_UP,             # Activate pullup resistor
#          motor1.isr_2) 

 
Controller = PID(encoder1,encoder2,motor1,motor2,sensor,0,.75,0,5)
while True:
    
    Controller.ball()
    Controller.Prop()
    utime.sleep(.05)
    
    